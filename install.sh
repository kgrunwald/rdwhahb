#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password your_password'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password your_password'

apt-get -y install python-pip python-dev mysql-server libmysqlclient-dev
pip install virtualenv virtualenvwrapper
mkdir -p /root/.virtualenvs

grep -q -F 'export WORKON_HOME=~/.virtualenvs' /root/.bashrc || echo 'export WORKON_HOME=~/.virtualenvs' >> ~/.bashrc

. /usr/local/bin/virtualenvwrapper.sh
grep -q -F '. /usr/local/bin/virtualenvwrapper.sh' /root/.bashrc || echo '. /usr/local/bin/virtualenvwrapper.sh' >> ~/.bashrc

cp $DIR/rdwhahb.conf /etc/init/rdwhahb.conf || :
cp $DIR/rdwhahb.service /etc/systemd/system/rdwhahb.service || :

if [ ! -d "/root/.virtualenvs/rdwhahb" ]; then
    mkdir -p /root/.virtualenvs/rdwhahb
    virtualenv /root/.virtualenvs/rdwhahb
fi

ln -sf `pwd ./` /srv/rdwhahb
mkdir -p /var/log/rdwhahb

pip install --upgrade pip
/root/.virtualenvs/rdwhahb/bin/pip install -r $DIR/server/api/requirements.txt

set +e
systemctl enable rdwhahb
systemctl daemon-reload
systemctl restart rdwhahb