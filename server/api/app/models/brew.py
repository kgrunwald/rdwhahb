from base import Base
from app import db
from flask_restful import fields
from controller import Controller
from datetime import timedelta
from cooler import Cooler


class Brew(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    style = db.Column(db.String(32))
    recipe = db.Column(db.String(32))
    yeast = db.Column(db.String(32))
    brew_date = db.Column(db.DateTime)
    carbonation_method = db.Column(db.Enum('BOTTLE', 'KEG'))
    controllers = db.relationship('Controller', cascade='delete')
    cooler_id = db.Column(db.Integer, db.ForeignKey('cooler.id'))
    cooler = db.relationship('Cooler', back_populates='brews')

    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'style': fields.String,
        'recipe': fields.String,
        'yeast': fields.String,
        'brewDate': fields.DateTime(dt_format='iso8601', attribute='brew_date'),
        'fermentationCompleteDate': fields.DateTime(dt_format='iso8601', attribute='fermentation_complete_date'),
        'readyDate': fields.DateTime(dt_format='iso8601', attribute='ready_date'),
        'carbonationMethod': fields.String(attribute='carbonation_method'),
        'controllers': fields.List(fields.Nested(Controller.fields)),
        'cooler': fields.Nested(Cooler.fields),
        'coolerId': fields.Integer(attribute='cooler_id'),
        'href': fields.String,
        'tempInRange': fields.Boolean(attribute='temp_in_range')
    }

    @property
    def fermentation_complete_date(self):
        controller_end = None
        for controller in self.controllers:
            if controller.type == 'FERMENTATION':
                controller_end = controller.projected_end_time
                break
        return controller_end

    @property
    def ready_date(self):
        controller_end = self.fermentation_complete_date
        if controller_end:
            delta = timedelta(weeks=2) if self.carbonation_method == 'BOTTLE' else timedelta(days=3)
            return controller_end + delta
        else:
            return None

    @property
    def active_controller(self):
        for controller in self.controllers:
            if controller.active:
                return controller
        return None

    @property
    def needs_cooling(self):
        controller = self.active_controller
        if controller:
            set_point = controller.current_set_point
            drift = controller.current_drift
            value = controller.sensor.value
            return value > set_point + drift
        return False

    @property
    def needs_heating(self):
        controller = self.active_controller
        if controller:
            set_point = controller.current_set_point
            drift = controller.current_drift
            value = controller.sensor.value
            return value < set_point - drift
        return False

    @property
    def temp_in_range(self):
        controller = self.active_controller
        return controller.temp_in_range if controller else None

    def in_range(self, pad_low=1, pad_high=1):
        controller = self.active_controller
        if controller:
            set_point = controller.current_set_point
            drift = controller.current_drift
            value = controller.sensor.value
            return (set_point - drift * pad_low) < value < (set_point + drift * pad_high)
        return None

    def __repr__(self):
        return '<Brew %r>' % self.name
