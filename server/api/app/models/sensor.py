from app import db
from base import Base
from flask_restful import fields
from datetime import datetime

MAX_DEFAULT = 150.0
MIN_DEFAULT = 20.0


class Sensor(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    units = db.Column(db.String(16))
    value = db.Column(db.Float, default=0.0)
    max_value = db.Column(db.Float, default=MAX_DEFAULT)
    min_value = db.Column(db.Float, default=MIN_DEFAULT)

    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'value': fields.Float,
        'units': fields.String,
        'maxValue': fields.Float(attribute='max_value', default=MAX_DEFAULT),
        'minValue': fields.Float(attribute='min_value', default=MIN_DEFAULT),
        'href': fields.String
    }

    def __repr__(self):
        return '<Sensor %r %r %r>' % (self.name, self.value, self.units)

    @staticmethod
    def set_value(sensor_id, value):
        sensor = Sensor.get(sensor_id)
        sensor.value = value
        Sensor.save(sensor)
        SensorLog.log_reading(sensor, value)
        return sensor


class SensorLog(Base):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    value = db.Column(db.Float)
    sensor = db.relationship('Sensor')
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))

    fields = {
        'id': fields.Integer,
        'timestamp': fields.DateTime(dt_format='iso8601'),
        'value': fields.Float(),
        'sensorId': fields.Integer(attribute='sensor_id')
    }

    @staticmethod
    def log_reading(sensor, value):
        log_entry = SensorLog()
        log_entry.timestamp = datetime.now()
        log_entry.value = value
        log_entry.sensor = sensor
        SensorLog.save(log_entry)
