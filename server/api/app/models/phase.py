from base import Base
from app import db
from flask_restful import fields
from datetime import datetime


class Phase(Base):
    id = db.Column(db.Integer, primary_key=True)
    set_point = db.Column(db.Float)
    max_drift = db.Column(db.Float, default=2)
    duration = db.Column(db.Integer)
    schedule_id = db.Column(db.Integer, db.ForeignKey('schedule.id'))
    schedule = db.relationship('Schedule', back_populates='phases')

    fields = {
        'id': fields.Integer,
        'setPoint': fields.Float(attribute='set_point'),
        'maxDrift': fields.Float(attribute='max_drift'),
        'duration': fields.Integer,
        'durationDays': fields.Float(attribute='duration_days'),
        'href': fields.String
    }

    @property
    def duration_days(self):
        return self.duration / (24 * 60) if self.duration is not None else None

    def __repr__(self):
        return '<Period %r %r %r>' % (self.start_date, self.end_date, self.set_point)

    @staticmethod
    def get_active(sensor_id):
        now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        sql = """
SELECT
  `period`.`id`
FROM
  `period`
INNER JOIN
  `schedule` ON `schedule`.`id` = `period`.`schedule_id`
 WHERE
  `period`.`start_date` <= '{0}'
      AND `period`.`end_date` >= '{0}'
      AND `schedule`.`sensor_id` = {1}
""".format(now, sensor_id)

        result = db.engine.execute(sql)
        periods = []
        for row in result:
            periods.append(Phase.get(row[0]))
        return periods
