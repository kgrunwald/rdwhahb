from base import Base
from app import db, app
from flask_restful import fields
from datetime import datetime
from werkzeug.exceptions import Conflict, InternalServerError
import requests
import commands

PI_URL = app.config['RPI_URL']


class Actuator(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    state = db.Column(db.Boolean, default=False)
    port_number = db.Column(db.Integer)
    controllers = db.relationship('Controller', back_populates='actuator')
    last_activate_time = db.Column(db.DateTime)
    last_deactivate_time = db.Column(db.DateTime)
    enabled = db.Column(db.Boolean, default=True)
    override = db.Column(db.Boolean, default=False)
    min_active_time = db.Column(db.Integer)
    cycle_delay = db.Column(db.Integer)
    type = db.Column(db.Enum('GPIO', 'M-POWER'), default='GPIO')
    role = db.Column(db.String(32))
    logs = db.relationship('ActuatorLog', backref="post", cascade="all, delete-orphan", lazy='dynamic')

    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'state': fields.Boolean,
        'portNumber': fields.Integer(attribute='port_number'),
        'href': fields.String,
        'minActiveTime': fields.Integer(attribute='min_active_time'),
        'cycleDelay': fields.Integer(attribute='cycle_delay'),
        'lastActivateTime': fields.DateTime(dt_format='iso8601', attribute='last_activate_time'),
        'lastDeactivateTime': fields.DateTime(dt_format='iso8601', attribute='last_deactivate_time'),
        'canActivate': fields.Boolean(attribute='can_activate'),
        'canDeactivate': fields.Boolean(attribute='can_deactivate'),
        'enabled': fields.Boolean(),
        'override': fields.Boolean(),
        'type': fields.String,
        'role': fields.String
    }

    def __repr__(self):
        return '<Actuator: %s  State: %r  Override: %r>' % (self.name, self.state, self.override)

    @property
    def can_activate(self):
        if self.override or not self.enabled:
            return False

        if self.state is True or self.cycle_delay is None or self.last_deactivate_time is None:
            return True

        elapsed = (datetime.now() - self.last_deactivate_time).total_seconds()
        return elapsed >= self.cycle_delay

    @property
    def can_deactivate(self):
        if self.override or not self.enabled:
            return False

        if self.state is False or self.min_active_time is None or self.last_activate_time is None:
            return True

        elapsed = (datetime.now() - self.last_activate_time).total_seconds()
        return elapsed >= self.min_active_time

    def enable(self):
        self.enabled = True
        return self

    def disable(self):
        self.enabled = False
        Actuator.set_state(self.id, False)
        return self

    @staticmethod
    def set_state(actuator_id, state, manual_override=False):
        actuator = Actuator.get(actuator_id)

        if not manual_override:
            if state and not actuator.can_activate:
                print 'Cannot activate %r' % actuator
                raise Conflict('Cannot activate actuator currently.')
            elif not state and not actuator.can_deactivate:
                print 'Cannot deactivate %r' % actuator
                raise Conflict('Cannot deactivate actuator currently.')
        else:
            print 'Manual override set.'

        res = Actuator._set_actuator_state(actuator, state, manual_override)
        print 'Set actuator state result: ' + str(res)
        actuator.state = state
        Actuator.save(actuator)

        return actuator

    @staticmethod
    def _set_actuator_state(actuator, state, manual_override):
        if not manual_override:
            if state and not actuator.state:
                actuator.last_activate_time = datetime.now()
            elif not state and actuator.state:
                actuator.last_deactivate_time = datetime.now()
            else:
                print 'Do not need to change actuator state'
                return False

        ActuatorLog.log_action(actuator, state, manual_override)

        if actuator.type == 'GPIO':
            r = requests.put(PI_URL + '/gpio/' + str(actuator.port_number), json={'state': state},
                             headers={'X-API-Key': '8fb96e57-3c54-4b1f-a111-5c4f59c8c135'})
            if r.json()['message'] != 'ok':
                raise InternalServerError('Failed setting actuator state in backend controller.')
        elif actuator.type == 'M-POWER':
            cmd = 'ssh mpower \'echo {} > /proc/power/relay{}\''.format(int(state), actuator.port_number)
            status, output = commands.getstatusoutput(cmd)
            if status != 0:
                raise Exception(output)

        return True


class ActuatorLog(Base):
    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime)
    state = db.Column(db.Boolean)
    actuator = db.relationship('Actuator', single_parent=True)
    actuator_id = db.Column(db.Integer, db.ForeignKey('actuator.id'))
    type = db.Column(db.Enum('CONTROLLER', 'MANUAL'))

    fields = {
        'id': fields.Integer,
        'timestamp': fields.DateTime(dt_format='iso8601'),
        'state': fields.Boolean(),
        'type': fields.String,
        'actuatorId': fields.Integer(attribute='actuator_id')
    }

    @staticmethod
    def log_action(actuator, state, manual_override):
        print 'Logging actuator action. Actuator: ' + str(actuator.id) + ' state: ' + str(state) + \
              ' manual_override: ' + str(manual_override)
        log_entry = ActuatorLog()
        log_entry.timestamp = datetime.now()
        log_entry.state = state
        log_entry.type = 'MANUAL' if manual_override else 'CONTROLLER'
        log_entry.actuator = actuator
        ActuatorLog.save(log_entry)
