from app import db
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import Conflict
from werkzeug.exceptions import NotFound
from app import app
from flask import url_for


class Base(db.Model):
    __abstract__ = True
    URL_BASE = app.config['URL_BASE']

    @staticmethod
    def save(actuator):
        try:
            db.session.add(actuator)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise Conflict(description='A sensor with that name already exists')

    @property
    def href(self):
        resource_name = self.__class__.__name__.lower()
        args = dict()
        args[resource_name + '_id'] = self.id
        return self.URL_BASE + url_for(resource_name, **args)

    @classmethod
    def all(cls):
        return cls.query.all()

    @classmethod
    def save(cls, instance):
        try:
            db.session.add(instance)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise Conflict(description='A %s with that name already exists' % cls.__name__)

    @classmethod
    def delete(cls, instance):
        try:
            db.session.delete(instance)
            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            raise Conflict(description='Could not delete provided entity.')

    @classmethod
    def get(cls, entity_id):
        entity = cls.query.get(entity_id)
        if entity is None:
            raise NotFound(description='Could not find ' + cls.__name__ + ' with id: ' + str(entity_id))
        return entity

    @classmethod
    def get_query(cls, **kwargs):
        entities = cls.query.filter_by(**kwargs).all()
        if entities is None:
            raise NotFound(description='Could not find ' + cls.__name__ + ' by query')
        return entities

    @classmethod
    def get_one_query(cls, **kwargs):
        entities = cls.query.filter_by(**kwargs).first()
        if entities is None:
            raise NotFound(description='Could not find one ' + cls.__name__ + ' by query')
        return entities

    @classmethod
    def get_between(cls, lower_key, lower_value, upper_key, upper_value):
        entities = cls.query.filter(lower_key <= lower_value).filter(upper_key >= upper_value)
        if entities is None:
            raise NotFound(description='Could not find ' + cls.__name__ + ' by query between')
        return entities
