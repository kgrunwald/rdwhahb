from base import Base
from app import db
from flask_restful import fields, url_for
from actuator import Actuator
from sensor import Sensor
from schedule import Schedule
from datetime import datetime, timedelta
from werkzeug.exceptions import NotAcceptable


class Controller(Base):
    id = db.Column(db.Integer, primary_key=True)
    start_time = db.Column(db.DateTime)
    actual_end_time = db.Column(db.DateTime)
    actuator_id = db.Column(db.Integer, db.ForeignKey('actuator.id'))
    actuator = db.relationship('Actuator', back_populates='controllers')
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))
    sensor = db.relationship('Sensor')
    schedule_id = db.Column(db.Integer, db.ForeignKey('schedule.id'))
    schedule = db.relationship('Schedule')
    brew_id = db.Column(db.Integer, db.ForeignKey('brew.id'))
    brew = db.relationship('Brew', back_populates='controllers')
    type = db.Column(db.Enum('FERMENTATION', 'CARBONATION', 'LAGER'))

    fields = {
        'id': fields.Integer,
        'startTime': fields.DateTime(dt_format='iso8601', attribute='start_time'),
        'projectedEndTime': fields.DateTime(dt_format='iso8601', attribute='projected_end_time'),
        'actualEndTime': fields.DateTime(dt_format='iso8601', attribute='actual_end_time'),
        'active': fields.Boolean,
        'phaseTimeRemaining': fields.Integer(attribute='phase_time_remaining'),
        'currentSetPoint': fields.Float(attribute='current_set_point'),
        'currentAcceptableDrift': fields.Float(attribute='current_drift'),
        'actuator': fields.Nested(Actuator.fields),
        'actuatorId': fields.Integer(attribute='actuator_id'),
        'sensor': fields.Nested(Sensor.fields),
        'sensorId': fields.Integer(attribute='sensor_id'),
        'schedule': fields.Nested(Schedule.fields),
        'scheduleId': fields.Integer(attribute='schedule_id'),
        'type': fields.String,
        'href': fields.String,
        'tempInRange': fields.Boolean(attribute='temp_in_range'),
        'complete': fields.Boolean
    }

    @property
    def projected_end_time(self):
        time = self.start_time
        schedule = self.schedule
        if time is None or schedule is None:
            return None

        for phase in schedule.phases:
            time = time + timedelta(0, phase.duration * 60)
        return time if self.active else None

    @property
    def active(self):
        return self.start_time is not None and self.actual_end_time is None

    @property
    def complete(self):
        phase, remaining = self._get_current_phase()
        return remaining is None

    @property
    def phase_time_remaining(self):
        phase, remaining = self._get_current_phase()
        return remaining if self.active else None

    @property
    def current_set_point(self):
        phase, remaining = self._get_current_phase()
        return phase.set_point if phase is not None and self.active else None

    @property
    def current_drift(self):
        phase, remaining = self._get_current_phase()
        return phase.max_drift if phase is not None and self.active else None

    @property
    def temp_in_range(self):
        if self.current_set_point is None or self.current_drift is None or not self.active:
            return False

        low = self.current_set_point - self.current_drift
        high = self.current_set_point + self.current_drift
        return low <= self.sensor.value <= high

    @property
    def href(self):
        args = dict()
        args['controller_id'] = self.id
        args['brew'] = self.brew_id
        return self.URL_BASE + url_for('controller', controller_id=self.id, brew_id=self.brew_id)

    def start_controller(self, force_restart=False):
        if self.start_time is None or force_restart:
            for controller in self.actuator.controllers:
                if controller is not self and controller.active:
                    raise NotAcceptable('Cannot start controller. '
                                        'Another controller with this actuator is already running.')
            self.start_time = datetime.now()
            self.actual_end_time = None
        return self

    def _get_current_phase(self):
        if self.start_time is None:
            return None, 0

        elapsed_minutes = (datetime.now() - self.start_time).total_seconds() / 60
        phase_time = 0
        for phase in self.schedule.phases:
            phase_time += phase.duration
            if elapsed_minutes < phase_time:
                return phase, phase_time - elapsed_minutes

        if len(self.schedule.phases):
            return self.schedule.phases[-1], None

        return None, 0

    def __repr__(self):
        return '<Controller %r %r>' % (self.id, self.type)
