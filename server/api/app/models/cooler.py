from base import Base
from app import db, app
from flask_restful import fields
from actuator import Actuator
from sensor import Sensor


# from brew import Brew


class Cooler(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    actuator_id = db.Column(db.Integer, db.ForeignKey('actuator.id'))
    actuator = db.relationship('Actuator')
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))
    sensor = db.relationship('Sensor')
    brews = db.relationship('Brew', back_populates='cooler')
    absolute_max = db.Column(db.Float, default=85.0)
    absolute_min = db.Column(db.Float, default=40.0)
    max_drift = db.Column(db.Float, default=10)

    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'actuator': fields.Nested(Actuator.fields),
        'actuatorId': fields.Integer(attribute='actuator_id'),
        'sensor': fields.Nested(Sensor.fields),
        'sensorId': fields.Integer(attribute='sensor_id'),
        'setPoint': fields.Float(attribute='set_point'),
        'href': fields.String,
        'maxDrift': fields.Integer(attribute='max_drift'),
        'absoluteMax': fields.Float(attribute='absolute_max'),
        'absoluteMin': fields.Float(attribute='absolute_min'),
        'canActivate': fields.Boolean(attribute='can_activate'),
        'canDeactivate': fields.Boolean(attribute='can_deactivate'),
        'tempInRange': fields.Boolean(attribute='temp_in_range')
        # 'brews': fields.List(fields.Nested(Brew.fields))
    }

    @property
    def set_point(self):
        point = 1000
        for brew in self.brews:
            for controller in brew.controllers:
                new_point = controller.current_set_point
                if controller.active and new_point < point:
                    point = new_point

        return point if point != 1000 else None

    @property
    def below_absolute_max(self):
        return self.sensor.value < self.absolute_max

    @property
    def above_absolute_min(self):
        return self.sensor.value > self.absolute_min

    @property
    def can_activate(self):
        return self.above_absolute_min and self.actuator.can_activate

    @property
    def can_deactivate(self):
        return self.below_absolute_max and self.actuator.can_deactivate

    @property
    def temp_in_range(self):
        if self.set_point is None or self.max_drift is None:
            return False
        
        low = self.set_point - self.max_drift
        high = self.set_point + self.max_drift
        return low <= self.sensor.value <= high

    def brews_in_range(self, pad_low=1, pad_high=1):
        for brew in self.brews:
            if not brew.in_range(pad_low=pad_low, pad_high=pad_high):
                return False
        return True

    @property
    def brews_cooling(self):
        for brew in self.brews:
            if not brew.needs_cooling:
                return False
        return True

    @property
    def brews_heating(self):
        for brew in self.brews:
            if not brew.needs_heating:
                return False
        return True

    def __repr__(self):
        return '<Cooler %r %r>' % (self.name, self.id)
