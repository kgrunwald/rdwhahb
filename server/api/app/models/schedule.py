from base import Base
from app import db
from flask_restful import fields
from phase import Phase


class Schedule(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    type = db.Column(db.Enum('FERMENTATION', 'CARBONATION', 'LAGER'))
    phases = db.relationship('Phase', back_populates='schedule')

    fields = {
        'id': fields.Integer,
        'name': fields.String,
        'type': fields.String,
        'phases': fields.List(fields.Nested(Phase.fields)),
        'href': fields.String
    }

    def __repr__(self):
        return '<Schedule %r %r>' % (self.id, self.name)
