from base import Base
from app import db
from flask_restful import fields
from flask_security import UserMixin, RoleMixin, SQLAlchemyUserDatastore

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(Base, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    fields = {
        'name': fields.String,
        'description': fields.String
    }

    def __repr__(self):
        return '<Role %r>' % self.name


class UserProfile(Base):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', back_populates='profile')

    fields = {
        'id': fields.Integer,
        'firstName': fields.String(attribute='first_name'),
        'lastName': fields.String(attribute='last_name')
    }


class User(Base, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(255))
    current_login_ip = db.Column(db.String(255))
    login_count = db.Column(db.Integer)

    profile = db.relationship('UserProfile', uselist=False, cascade='delete')

    fields = {
        'id': fields.Integer,
        'email': fields.String,
        'active': fields.Boolean,
        'admin': fields.Boolean,
        'roles': fields.List(fields.Nested(Role.fields)),
        'lastLogin': fields.DateTime(attribute='last_login_at', dt_format='iso8601'),
        'currentLogin': fields.DateTime(attribute='current_login_at', dt_format='iso8601'),
        'lastLoginIp': fields.String(attribute='last_login_ip'),
        'currentLoginIp': fields.String(attribute='current_login_ip'),
        'loginCount': fields.Integer(attribute='login_count'),
        'accessToken': fields.String,
        'profile': fields.Nested(UserProfile.fields)
    }

    @classmethod
    def load(cls, email):
        return User.get_one_query(email=email)

    @property
    def admin(self):
        for role in self.roles:
            if role.name == 'admin':
                return True
        return False

    def __repr__(self):
        return '<User %r %r>' % (self.email, self.id)


user_datastore = SQLAlchemyUserDatastore(db, User, Role)
