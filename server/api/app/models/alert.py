from base import Base
from app import db
from flask_restful import fields


class Alert(Base):
    id = db.Column(db.Integer, primary_key=True)
    test_status = db.Column(db.String(64))
    name = db.Column(db.String(128))
    status_code = db.Column(db.Integer())
    url = db.Column(db.String(256))
    alert_state = db.Column(db.Enum('NEW', 'ACKNOWLEDGED', 'RESOLVED'))
    timestamp = db.Column(db.DateTime)
    acknowledged_at = db.Column(db.DateTime)
    resolved_at = db.Column(db.DateTime)
    read = db.Column(db.Boolean, default=False)

    fields = {
        'id': fields.Integer,
        'testStatus': fields.String(attribute='test_status'),
        'name': fields.String,
        'statusCode': fields.Integer(attribute='status_code'),
        'url': fields.String,
        'alertState': fields.String(attribute='alert_state'),
        'timestamp': fields.DateTime(dt_format='iso8601'),
        'acknowledgedAt': fields.DateTime(dt_format='iso8601', attribute='acknowledged_at'),
        'resolvedAt': fields.DateTime(dt_format='iso8601', attribute='resolved_at'),
        'read': fields.Boolean
    }
