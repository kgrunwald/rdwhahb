from flask_restful import marshal_with
from app import api, db, app
from app.models import Actuator as ActuatorModel, ActuatorLog
from base import Base
from authorization import jwt_required
from datetime import datetime, timedelta
from dateutil import parser as date_parser


class ActuatorHistory(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ActuatorLog.fields)
    def get(self, actuator_id):
        self._string_arg('startTime', required=False, location='args')
        self._string_arg('endTime', required=False, location='args')
        self._string_arg('order', required=False, location='args')
        self._float_arg('limit', required=False, location='args')
        args = self.parser.parse_args()

        if args['startTime']:
            start_time = date_parser.parse(args['startTime'])
        else:
            start_time = datetime.now() - timedelta(hours=6)

        if args['endTime']:
            end_time = date_parser.parse(args['endTime'])
        else:
            end_time = datetime.now()

        query = ActuatorLog.query.filter(ActuatorLog.timestamp >= start_time, ActuatorLog.timestamp <= end_time,
                                         ActuatorLog.actuator_id == actuator_id)

        if args['order'] and args['order'] == 'DESC':
            query = query.order_by(ActuatorLog.timestamp.desc())

        if args['limit']:
            print 'limit: ', int(args['limit'])
            query = query.limit(int(args['limit']))

        return query.all()


class ActuatorState(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self, actuator_id):
        actuator = ActuatorModel.get(actuator_id)
        return actuator.state

    @marshal_with(ActuatorModel.fields)
    @jwt_required()
    def post(self, actuator_id):
        self._string_arg('state')
        args = self.parser.parse_args()
        actuator = ActuatorModel.get(actuator_id)
        Base.set_model_attributes(actuator, args)
        ActuatorModel.save(actuator)
        return actuator


class Actuator(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ActuatorModel.fields)
    def get(self, actuator_id):
        return ActuatorModel.get(actuator_id)

    @marshal_with(ActuatorModel.fields)
    @jwt_required()
    def put(self, actuator_id):
        self._float_arg('portNumber', required=False)
        self._bool_arg('enabled', required=False)
        self._bool_arg('state', required=False)
        self._bool_arg('override', required=False)
        self._float_arg('cycleDelay', required=False)
        self._float_arg('minActiveTime', required=False)
        self._string_arg('name', required=False)
        self._string_arg('role', required=False)
        self._string_arg('type', required=False)
        args = self.parser.parse_args()
        actuator = ActuatorModel.get(actuator_id)

        Base.set_model_attributes(actuator, args)

        if actuator.enabled:
            actuator.enable()
        else:
            actuator.disable()

        if args['state'] is not None:
            print 'Setting actuator state. Actuator id: ' + str(actuator_id) + ' state: ' + str(actuator.state)
            ActuatorModel.set_state(actuator_id, actuator.state, manual_override=True)
        ActuatorModel.save(actuator)
        return actuator

    @jwt_required()
    def delete(self, actuator_id):
        actuator = ActuatorModel.get(actuator_id)
        db.session.delete(actuator)
        db.session.commit()
        return {'message': 'ok'}


class ActuatorList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ActuatorModel.fields)
    def get(self):
        self._string_arg('role', required=False, location='args')
        args = self.parser.parse_args()
        if 'role' in args.keys() and args['role'] is not None:
            return ActuatorModel.get_query(role=args['role'])

        return ActuatorModel.all()

    @marshal_with(ActuatorModel.fields)
    @jwt_required()
    def post(self):
        self._string_arg('name')
        self._bool_arg('state', required=False)
        self._string_arg('portNumber')
        self._string_arg('role', required=True)
        self._string_arg('type', required=True)
        args = self.parser.parse_args()
        actuator = ActuatorModel()
        Base.set_model_attributes(actuator, args)
        ActuatorModel.save(actuator)

        return actuator, 201


api.add_resource(ActuatorList, '/actuators')
api.add_resource(Actuator, '/actuators/<int:actuator_id>')
api.add_resource(ActuatorState, '/actuators/<int:actuator_id>/state')
api.add_resource(ActuatorHistory, '/actuators/<int:actuator_id>/history')
