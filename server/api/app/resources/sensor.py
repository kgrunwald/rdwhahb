from flask_restful import marshal_with
from app import api, db, app
from app.models import Sensor as SensorModel, Controller as ControllerModel, Actuator as ActuatorModel, \
    Cooler as CoolerModel, SensorLog
from base import Base
from werkzeug.exceptions import NotFound, Conflict
from authorization import jwt_required, api_key_required
from datetime import datetime, timedelta
from dateutil import parser as date_parser


COOL_PER_SEC = 0.2 / 60.
HEAT_PER_SEC = 0.75 / 60.


def get_heat_brew(controller, value):
    set_point = controller.current_set_point
    acceptable_drift = controller.current_drift
    low_threshold_trigger = set_point - acceptable_drift
    low_threshold = set_point - 0.85 * acceptable_drift
    if value < low_threshold_trigger:
        return True, True
    elif value < low_threshold:
        return False, True
    else:
        return False, False


def get_active_controller(controllers):
    for controller in controllers:
        if controller.active:
            return controller

    return None


def should_activate_heater(brew, value):
    if not brew.cooler.below_absolute_max:
        return False

    time = max(brew.active_controller.actuator.min_active_time, 60)
    controller = brew.active_controller
    if value + HEAT_PER_SEC * time < controller.current_set_point:
        return True

    if not brew.cooler.above_absolute_min:
        return True

    return False


def should_deactivate_heater(brew, value):
    if not brew.cooler.above_absolute_min:
        return False

    controller = brew.active_controller
    if value + HEAT_PER_SEC * 60 > controller.current_set_point - controller.current_drift * 0.5:
        return True

    if not brew.cooler.below_absolute_max:
        return True

    return False


def should_activate_cooler(cooler, value):
    min_brew_temp = min([x.active_controller.sensor.value if x.active_controller else 500 for x in cooler.brews])
    if min_brew_temp is None or cooler.set_point is None:  # There are no active controllers
        return False

    time = max(cooler.actuator.min_active_time, 60)
    if min_brew_temp and min_brew_temp - COOL_PER_SEC * time > cooler.set_point - 0.15:
        return True

    if value - cooler.set_point > cooler.max_drift and cooler.brews_in_range(pad_low=0):
        return True

    return cooler.brews_cooling


def should_deactivate_cooler(cooler, value):
    # If we keep cooling, we will overshoot our range.
    controller = None
    min_brew_temp = 500
    target = None
    for brew in cooler.brews:
        tmp_controller = brew.active_controller
        if tmp_controller and tmp_controller.sensor.value < min_brew_temp:
            controller = tmp_controller
            min_brew_temp = controller.sensor.value
            target = controller.current_set_point + controller.current_drift * 0.1

    if min_brew_temp == 500 or target is None:  # No active controllers
        return True

    if controller and min_brew_temp - COOL_PER_SEC * 60 < target:
        return True

    if cooler.brews_in_range(pad_high=0.0):
        return True

    if cooler.brews_heating:
        return True

    return cooler.set_point - value > cooler.max_drift


def set_sensor_value(sensor_id, value):
    sensor = SensorModel.set_value(sensor_id, value)

    try:
        # Check to see if we need to turn on the beer heater
        controllers = ControllerModel.get_query(sensor_id=sensor_id)
        controller = get_active_controller(controllers)
        if controller:
            brew = controller.brew
            actuator = controller.actuator
            if not actuator.state and actuator.can_activate and should_activate_heater(brew, value):
                ActuatorModel.set_state(controller.actuator.id, True)
            elif actuator.state and actuator.can_deactivate and should_deactivate_heater(brew, value):
                ActuatorModel.set_state(controller.actuator.id, False)

        cooler = CoolerModel.get_one_query(sensor_id=sensor_id)
        if cooler:
            actuator = cooler.actuator
            if not actuator.state and actuator.can_activate and should_activate_cooler(cooler, value):
                ActuatorModel.set_state(cooler.actuator_id, True)
            elif actuator.state and actuator.can_deactivate and should_deactivate_cooler(cooler, value):
                ActuatorModel.set_state(cooler.actuator_id, False)
    except Conflict:
        print 'Cannot set sensor status due to actuator rules.'
    except NotFound:
        pass

    return sensor


class SensorHistory(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(SensorLog.fields)
    def get(self, sensor_id):
        self._string_arg('startTime', required=False, location='args')
        self._string_arg('endTime', required=False, location='args')
        self._string_arg('order', required=False, location='args')
        self._float_arg('limit', required=False, location='args')
        args = self.parser.parse_args()

        if args['startTime']:
            start_time = date_parser.parse(args['startTime'])
        else:
            start_time = datetime.now() - timedelta(hours=6)

        if args['endTime']:
            end_time = date_parser.parse(args['endTime'])
        else:
            end_time = datetime.now()

        query = SensorLog.query.filter(SensorLog.timestamp >= start_time, SensorLog.timestamp <= end_time,
                                       SensorLog.sensor_id == sensor_id)

        if args['order'] and args['order'] == 'DESC':
            query = query.order_by(SensorLog.timestamp.desc())

        if args['limit']:
            print 'limit: ', int(args['limit'])
            query = query.limit(int(args['limit']))

        return query.all()


class SensorValue(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self, sensor_id):
        sensor = SensorModel.get(sensor_id)
        return sensor.value

    @marshal_with(SensorModel.fields)
    @api_key_required
    def post(self, sensor_id):
        self._float_arg('value')
        args = self.parser.parse_args()
        return set_sensor_value(sensor_id, args['value'])


class Sensor(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(SensorModel.fields)
    def get(self, sensor_id):
        return SensorModel.get(sensor_id)

    @marshal_with(SensorModel.fields)
    @jwt_required()
    def put(self, sensor_id):
        self._string_arg('name', required=False)
        self._string_arg('units', required=False)
        self._float_arg('minValue', required=False)
        self._float_arg('maxValue', required=False)
        args = self.parser.parse_args()
        sensor = SensorModel.get(sensor_id)
        self.set_model_attributes(sensor, args)
        SensorModel.save(sensor)
        return sensor

    @jwt_required()
    def delete(self, sensor_id):
        sensor = SensorModel.get(sensor_id)
        db.session.delete(sensor)
        db.session.commit()
        return {'message': 'ok'}


class SensorList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(SensorModel.fields)
    def get(self):
        return SensorModel.all()

    @marshal_with(SensorModel.fields)
    @jwt_required()
    def post(self):
        self._string_arg('name')
        self._string_arg('units')
        self._string_arg('maxValue')
        self._string_arg('minValue')
        args = self.parser.parse_args()
        sensor = SensorModel()
        Base.set_model_attributes(sensor, args)
        SensorModel.save(sensor)

        return sensor, 201


api.add_resource(SensorList, '/sensors')
api.add_resource(Sensor, '/sensors/<int:sensor_id>')
api.add_resource(SensorValue, '/sensors/<int:sensor_id>/value')
api.add_resource(SensorHistory, '/sensors/<int:sensor_id>/history')
