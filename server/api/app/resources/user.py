from base import Base
from app import api
from flask_restful import marshal_with
from app.models import User as UserModel, UserProfile as ProfileModel, Role as RoleModel
from werkzeug.security import generate_password_hash
from werkzeug.exceptions import Unauthorized, Conflict
from sqlalchemy.exc import IntegrityError
from authorization import admin_only, jwt_required, current_identity, jwt


class RoleList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(RoleModel.fields)
    @admin_only
    def get(self):
        return RoleModel.all()

    @marshal_with(RoleModel.fields)
    @admin_only
    def post(self):
        self._string_arg('name')
        self._string_arg('description')
        args = self.parser.parse_args()
        r = RoleModel()
        Base.set_model_attributes(r, args)
        RoleModel.save(r)
        return r


class UserAdmin(Base):
    def __init__(self):
        Base.__init__(self)

    @admin_only
    def delete(self, user_id):
        u = UserModel.get(user_id)
        u.active = False
        UserModel.save(u)
        return {'message': 'user updated'}

    @admin_only
    def put(self, user_id):
        self._string_arg('email', required=False)
        self._string_arg('password', required=False)
        self._bool_arg('admin', required=False)
        self._bool_arg('active', required=False)
        args = self.parser.parse_args()
        u = UserModel.get(user_id)
        self.set_model_attributes(u, args)

        if args.admin:
            r = RoleModel.get_one_query(name='admin')
            u.roles = [r]
        UserModel.save(u)
        return {'message': 'user updated'}


class UserValidation(Base):
    def __init__(self):
        Base.__init__(self)

    @admin_only
    def get(self):
        self._string_arg('email', location='args')
        args = self.parser.parse_args()
        u = UserModel.load(args['email'])
        if u:
            return {'message': 'A user with that email already exists'}, 409
        else:
            return {'message': 'no conflicts found'}


class UserToken(Base):
    def __init__(self):
        Base.__init__(self)

    @jwt_required()
    def get(self, user_id):
        user = UserModel.get(user_id)
        token = jwt.jwt_encode_callback(user)
        return {'accessToken': token.decode('utf-8')}


class User(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(UserModel.fields)
    @jwt_required()
    def get(self, user_id):
        if not current_identity.admin and current_identity.id != user_id:
            raise Unauthorized('You cannot modify another user')
        return UserModel.get(user_id)

    @admin_only
    def delete(self, user_id):
        u = UserModel.get(user_id)
        UserModel.delete(u)
        return {'message': 'user deleted'}

    @jwt_required()
    def put(self, user_id):
        if not (current_identity.admin or current_identity.id == user_id):
            raise Unauthorized('You cannot modify another user')

        self._string_arg('email', required=False)
        self._string_arg('password', required=False)
        self._string_arg('firstName', required=False)
        self._string_arg('lastName', required=False)
        self._custom_arg('roles', arg_type=list, required=False, location='json')
        args = self.parser.parse_args()
        u = UserModel.get(user_id)
        self.set_model_attributes(u, args, skip=['roles'])
        if args.password:
            u.password = generate_password_hash(args.password)

        if args['roles'] is not None:
            u.roles = []
            for role in args['roles']:
                r = RoleModel.get_one_query(name=role)
                u.roles.append(r)

        p = u.profile
        self.set_model_attributes(p, args)
        ProfileModel.save(p)
        UserModel.save(u)
        return {'message': 'user updated'}


class UserList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(UserModel.fields)
    @admin_only
    def get(self):
        self._string_arg('email', location='args', required=False)
        args = self.parser.parse_args()
        if args['email']:
            return UserModel.load(args['email'])

        return UserModel.all()

    @marshal_with(UserModel.fields)
    # @admin_only
    def post(self):
        self._string_arg('email')
        self._string_arg('password')
        self._string_arg('firstName')
        self._string_arg('lastName')
        self._custom_arg('roles', arg_type=list, location='json', required=False)
        args = self.parser.parse_args()
        u = UserModel()
        u.email = args.email
        u.password = generate_password_hash(args.password)
        u.active = True
        u.roles = []
        if args['roles']:
            for role in args['roles']:
                r = RoleModel.get_one_query(name=role)
                u.roles.append(r)

        p = ProfileModel()
        p.first_name = args['firstName']
        p.last_name = args['lastName']
        p.user = u
        try:
            ProfileModel.save(p)
            UserModel.save(u)
        except Exception:
            raise Conflict('A user with that email already exists.')

        return u, 201


api.add_resource(UserList, '/users')
api.add_resource(UserValidation, '/users/validate')
api.add_resource(User, '/users/<int:user_id>')
api.add_resource(UserAdmin, '/users/<int:user_id>/admin')
api.add_resource(UserToken, '/users/<int:user_id>/token')
api.add_resource(RoleList, '/roles')
