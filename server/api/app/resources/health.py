from app import api, app
from base import Base
from app.models import Brew, SensorLog, Alert as AlertModel
from flask_restful import marshal, marshal_with
from werkzeug.exceptions import Unauthorized
from datetime import datetime
from authorization import jwt_required
import commands
import hashlib


class MPowerHealth(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self):
        cmd = 'ssh mpower \'echo hello\''
        status, output = commands.getstatusoutput(cmd)
        if status != 0 or output != 'hello':
            raise Exception(output)
        return {'message': 'ok', 'status': status, 'output': output}


class BrewHealth(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self, brew_id):
        factor = app.config['TEMP_ERROR_FACTOR']
        brew = Brew.get(brew_id)
        if not brew.in_range(pad_high=factor, pad_low=factor):
            return marshal(brew, Brew.fields), 200
        return None, 204


class SensorHealth(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self, sensor_id):
        time_range = app.config['SENSOR_ERROR_TIME_RANGE']
        expected = app.config['SENSOR_ERROR_EXPECTED_READINGS']
        cutoff = datetime.now() - time_range
        log_entries = SensorLog.query.filter(SensorLog.timestamp >= cutoff, SensorLog.sensor_id == sensor_id).all()
        count = len(log_entries)
        if count < expected:
            return {'message': 'Expected: ' + str(expected) + ' updates but only got: ' + str(count)}, 200
        return None, 204


class StatusAlert(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(AlertModel.fields)
    def get(self, alert_id):
        return AlertModel.get(alert_id)

    @marshal_with(AlertModel.fields)
    @jwt_required()
    def put(self, alert_id):
        self._string_arg('alertState', required=False)
        self._bool_arg('read', required=False)
        args = self.parser.parse_args()

        alert = AlertModel.get(alert_id)
        self.set_model_attributes(alert, args)
        AlertModel.save(alert)
        return alert


class StatusAlerts(Base):
    def __init__(self):
        Base.__init__(self)

    def head(self):
        return {'status': 'ok'}

    @marshal_with(AlertModel.fields)
    def get(self):
        self._float_arg('limit', required=False, default=10, location='args')
        self._string_arg('order', required=False, default='DESC', location='args')
        args = self.parser.parse_args()
        query = AlertModel.query
        if args['order'] == 'DESC':
            query = query.order_by(AlertModel.timestamp.desc())
        else:
            query = query.order_by(AlertModel.timestamp.asc())

        query = query.limit(int(args['limit']))

        return query.all()

    @marshal_with(AlertModel.fields)
    def post(self):
        self._string_arg('Token')
        self._string_arg('Status')
        self._string_arg('Name', required=False)
        self._float_arg('StatusCode', required=False)
        self._string_arg('URL', required=False)
        args = self.parser.parse_args()

        user_name = app.config['STATUS_CAKE_USERNAME']
        api_key = app.config['STATUS_CAKE_API_KEY']
        m = hashlib.md5()
        m.update(user_name + api_key)
        token = m.hexdigest()

        if args['Token'] != token:
            raise Unauthorized('Invalid token')

        alert = AlertModel.query.filter(AlertModel.name == args['Name'], AlertModel.test_status == 'Down') \
            .order_by(AlertModel.id.desc()).limit(1).first()
        if alert is None:
            alert = AlertModel()
            alert.timestamp = datetime.now()
        alert.test_status = args['Status']
        alert.name = args['Name']
        alert.status_code = args['StatusCode']
        alert.url = args['URL']
        if args['Status'] == 'Down':
            alert.alert_state = 'NEW'
        else:
            alert.alert_state = 'RESOLVED'
            alert.resolved_at = datetime.now()
        AlertModel.save(alert)
        return alert


class Ping(Base):
    def __init__(self):
        Base.__init__(self)

    def get(self):
        return {'message': 'pong'}


api.add_resource(Ping, '/ping')
api.add_resource(BrewHealth, '/health/brews/<int:brew_id>')
api.add_resource(SensorHealth, '/health/sensors/<int:sensor_id>')
api.add_resource(MPowerHealth, '/health/actuators/mpower')
api.add_resource(StatusAlerts, '/health/alerts')
api.add_resource(StatusAlert, '/health/alerts/<int:alert_id>')
