from werkzeug.exceptions import Unauthorized
from werkzeug.security import safe_str_cmp, check_password_hash
from app import app, api
from functools import wraps
from flask import request, jsonify
from flask_jwt import JWT, jwt_required, current_identity
from flask_restful import marshal
from app.models import User
from datetime import datetime
from base import Base
import json
import time
import base64


def api_key_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('X-API-Key') is None:
            raise Unauthorized('API Key not provided.')
        if not safe_str_cmp(request.headers.get('X-API-Key'), app.config['API_KEY']):
            raise Unauthorized('API Key not provided.')
        return f(*args, **kwargs)

    return decorated_function


def admin_only(f):
    @wraps(f)
    @jwt_required()
    def decorated_function(*args, **kwargs):
        if not current_identity.admin:
            raise Unauthorized('Must be an admin to perform this action.')
        return f(*args, **kwargs)

    return decorated_function


def __authenticate(email, password):
    user = User.load(email)
    if user and user.active and check_password_hash(user.password, password):
        user.last_login_at = user.current_login_at
        user.current_login_at = datetime.now()
        user.last_login_ip = user.current_login_ip
        user.current_login_ip = request.remote_addr
        User.save(user)
        return user


def __identity(payload):
    user_id = payload['identity']
    return User.get(user_id)


def __auth_response(access_token, identity):
    identity.accessToken = access_token.decode('utf-8')
    return jsonify(marshal(identity, User.fields))


def __payload_handler(identity):
    iat = datetime.utcnow()
    exp = iat + app.config.get('JWT_EXPIRATION_DELTA')
    nbf = iat + app.config.get('JWT_NOT_BEFORE_DELTA')
    id_num = getattr(identity, 'id') or identity['id']
    return {'exp': exp, 'iat': iat, 'nbf': nbf, 'identity': id_num, 'admin': identity.admin}


def _verify_token(token):
    try:
        token = token.split('.')
        decoded = json.loads(base64.b64decode(token[1]))
        if time.mktime(datetime.now().timetuple()) > decoded['exp']:
            return False
        return True
    except:
        raise ValueError('Invalid token')


class AuthVerifyToken(Base):
    def __init__(self):
        Base.__init__(self)

    def post(self):
        self._string_arg('accessToken', required=True)
        args = self.parser.parse_args()
        return {'isValid': _verify_token(args['accessToken'])}, 200

jwt = JWT(app, __authenticate, __identity)
jwt.auth_response_callback = __auth_response
jwt.jwt_payload_callback = __payload_handler

api.add_resource(AuthVerifyToken, '/auth/verifyToken')