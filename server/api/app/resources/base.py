from flask_restful import Resource, reqparse
from datetime import datetime
import re


class Base(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    def __add_arg(self, name, arg_type, help_str, required, location=None, default=None):
        if location is None:
            location = ['json', 'form', 'args']
        help_val = name + " was not provided" if help_str is None else help_str
        self.parser.add_argument(name, type=arg_type, help=help_val, required=required, location=location,
                                 default=default)

    def _string_arg(self, name, help_str=None, required=True, location=None, default=None):
        self.__add_arg(name, str, help_str, required, location, default)

    def _bool_arg(self, name, help_str=None, required=True, location=None, default=None):
        self.__add_arg(name, bool, help_str, required, location, default)

    def _float_arg(self, name, help_str=None, required=True, location=None, default=None):
        self.__add_arg(name, float, help_str, required, location, default)

    def _custom_arg(self, name, arg_type, help_str=None, required=True, location=None, default=None):
        self.__add_arg(name, arg_type, help_str, required, location, default)

    @staticmethod
    def convert_to_underscore(name):
        s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()

    @staticmethod
    def set_model_attributes(model, attrs, dates=None, skip=None):
        json_fields = model.fields
        model_attrs = json_fields.keys()
        for key, value in attrs.iteritems():
            if key == 'id':
                continue
            if value is None:
                continue
            if key not in model_attrs or skip and key in skip:
                continue
            if dates is not None and key in dates:
                value = datetime.strptime(value, '%Y-%m-%dT%H:%M:%S')
            setattr(model, Base.convert_to_underscore(key), value)
        return model
