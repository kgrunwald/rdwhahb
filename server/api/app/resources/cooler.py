from flask_restful import marshal_with
from app import api, db
from app.models import Cooler as CoolerModel, Sensor as SensorModel, Actuator as ActuatorModel, Brew as BrewModel
from werkzeug.exceptions import NotFound
from base import Base
import re
from authorization import jwt_required


def convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class Cooler(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(CoolerModel.fields)
    def get(self, cooler_id):
        return CoolerModel.get(cooler_id)

    @marshal_with(CoolerModel.fields)
    @jwt_required()
    def put(self, cooler_id):
        self._string_arg('name', required=False)
        self._float_arg('maxDrift', required=False)
        self._float_arg('sensorId', required=False)
        self._float_arg('actuatorId', required=False)
        args = self.parser.parse_args()
        cooler = CoolerModel.get(cooler_id)
        Base.set_model_attributes(cooler, args)
        CoolerModel.save(cooler)
        return cooler

    @jwt_required()
    def delete(self, cooler_id):
        cooler = CoolerModel.get(cooler_id)
        CoolerModel.delete(cooler)
        return {'message': 'ok'}


class CoolerBrews(Base):
    def __init__(self):
        Base.__init__(self)

    def delete(self, cooler_id):
        self._float_arg('brewId')
        args = self.parser.parse_args()
        brew = BrewModel.get(args['brewId'])
        if brew.cooler_id != cooler_id:
            raise NotFound('Brew id: ' + args['brewId'] + ' is not in cooler: ' + cooler_id)

        brew.cooler_id = None
        BrewModel.save(brew)
        return {'message': 'Brew removed from cooler'}, 200

    @marshal_with(BrewModel.fields)
    def get(self, cooler_id):
        return CoolerModel.get(cooler_id).brews

    @marshal_with(BrewModel.fields)
    @jwt_required()
    def post(self, cooler_id):
        self._float_arg('brewId')
        args = self.parser.parse_args()
        brew = BrewModel.get(args['brewId'])
        brew.cooler_id = cooler_id
        BrewModel.save(brew)

        return brew, 200


class CoolerList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(CoolerModel.fields)
    def get(self):
        return CoolerModel.all()

    @marshal_with(CoolerModel.fields)
    @jwt_required()
    def post(self):
        self._string_arg('name')
        self._float_arg('sensorId')
        self._float_arg('actuatorId')
        self._float_arg('maxDrift', required=False)
        args = self.parser.parse_args()
        cooler = CoolerModel()
        Base.set_model_attributes(cooler, args)
        CoolerModel.save(cooler)
        return cooler, 201


api.add_resource(CoolerList, '/coolers', endpoint='cooler_list')
api.add_resource(Cooler, '/coolers/<int:cooler_id>', endpoint='cooler')
api.add_resource(CoolerBrews, '/coolers/<int:cooler_id>/brews')
