from flask import request
from flask_restful import marshal_with
from app import api
from app.models import Schedule as ScheduleModel, Phase as PhaseModel
from base import Base
from authorization import jwt_required


def phase_list(value):
    try:
        x = []
        for phase_dict in value:
            keys = phase_dict.keys()
            if ('duration' not in keys and 'durationDays' not in keys) or 'setPoint' not in keys:
                raise TypeError('A required key is not present')

            if phase_dict['setPoint'] is None or phase_dict['setPoint'] is None:
                raise TypeError('A key is None')

            phase = dict()
            if 'duration' in keys and phase_dict['duration'] is not None:
                duration = int(phase_dict['duration'])
            elif 'durationDays' in keys and phase_dict['durationDays'] is not None:
                duration = int(phase_dict['durationDays']) * 24 * 60
            else:
                raise TypeError('Duration not provided')
            phase['duration'] = int(duration)
            phase['setPoint'] = float(phase_dict['setPoint'])
            phase['maxDrift'] = float(phase_dict['maxDrift'])
            x.append(phase)
    except TypeError, e:
        # Raise a ValueError, and maybe give it a good error string
        raise ValueError(e.message)
    except:
        # Just in case you get more errors
        raise ValueError

    return x


class Phase(Base):
    @marshal_with(PhaseModel.fields)
    def get(self, phase_id):
        return PhaseModel.get(phase_id)

    @marshal_with(PhaseModel.fields)
    @jwt_required()
    def put(self, phase_id):
        self._float_arg('duration', required=False)
        self._float_arg('maxDrift', required=False)
        self._float_arg('setPoint', required=False)
        args = self.parser.parse_args()
        phase = PhaseModel.get(phase_id)
        Base.set_model_attributes(phase, args)
        PhaseModel.save(phase)
        return phase


class SchedulePhases(Base):
    @marshal_with(PhaseModel.fields)
    @jwt_required()
    def post(self, schedule_id):
        self._float_arg('duration')
        self._float_arg('maxDrift')
        self._float_arg('setPoint')
        args = self.parser.parse_args()
        phase = PhaseModel()
        Base.set_model_attributes(phase, args)
        phase.schedule_id = schedule_id
        PhaseModel.save(phase)
        return phase

    @marshal_with(PhaseModel.fields)
    def get(self, schedule_id):
        return ScheduleModel.get(schedule_id).phases


class Schedule(Base):
    @marshal_with(ScheduleModel.fields)
    def get(self, schedule_id):
        return ScheduleModel.get(schedule_id)

    @marshal_with(ScheduleModel.fields)
    def put(self, schedule_id):
        self._string_arg('name', required=False, location='json')
        self._string_arg('type', required=False, location='json')
        self._custom_arg('phases', arg_type=phase_list, required=False, location='json')
        args = self.parser.parse_args()

        schedule = ScheduleModel.get(schedule_id)
        self.set_model_attributes(schedule, args, skip=['phases', 'id', 'href'])

        if 'phases' in args.keys():
            print args['phases']
            for existing in schedule.phases:
                PhaseModel.delete(existing)

            schedule.phases = []
            for phase in args['phases']:
                p = PhaseModel()
                p.set_point = phase['setPoint']
                p.max_drift = phase['maxDrift']
                p.duration = phase['duration']
                PhaseModel.save(p)
                schedule.phases.append(p)

        ScheduleModel.save(schedule)
        return schedule

    def delete(self, schedule_id):
        schedule = ScheduleModel.get(schedule_id)
        ScheduleModel.delete(schedule)
        return {'message': 'schedule deleted'}


class ScheduleList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ScheduleModel.fields)
    def get(self):
        return ScheduleModel.all()

    @marshal_with(ScheduleModel.fields)
    @jwt_required()
    def post(self):
        args = request.json
        schedule = ScheduleModel()
        self.set_model_attributes(schedule, args)
        schedule.phases = []

        if 'phases' in args.keys():
            for phase in args['phases']:
                p = PhaseModel()
                Base.set_model_attributes(p, phase)
                schedule.phases.append(p)

        ScheduleModel.save(schedule)
        return ScheduleModel.get(schedule.id), 201


api.add_resource(ScheduleList, '/schedules')
api.add_resource(Schedule, '/schedules/<int:schedule_id>')
api.add_resource(SchedulePhases, '/schedules/<int:schedule_id>/phases')
api.add_resource(Phase, '/phases/<int:phase_id>', endpoint='phase')
