from flask_restful import marshal_with
from app import api
from app.models import Brew as BrewModel
from base import Base
from authorization import jwt_required


class Brew(Base):
    def __init__(self):
        Base.__init__(self)

    @jwt_required()
    def delete(self, brew_id):
        brew = BrewModel.get(brew_id)
        BrewModel.delete(brew)
        return {'message': 'brew deleted'}

    @marshal_with(BrewModel.fields)
    def get(self, brew_id):
        return BrewModel.get(brew_id)

    @marshal_with(BrewModel.fields)
    @jwt_required()
    def put(self, brew_id):
        self._string_arg('carbonationMethod', required=False)
        self._string_arg('name', required=False)
        self._string_arg('recipe', required=False)
        self._string_arg('yeast', required=False)
        self._string_arg('style', required=False)
        args = self.parser.parse_args()
        brew = BrewModel.get(brew_id)
        Base.set_model_attributes(brew, args)
        BrewModel.save(brew)
        return brew


class BrewList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(BrewModel.fields)
    def get(self):
        return BrewModel.all()

    @marshal_with(BrewModel.fields)
    @jwt_required()
    def post(self):
        self._string_arg('name')
        self._string_arg('style')
        self._string_arg('recipe')
        self._string_arg('yeast')
        self._string_arg('brewDate')
        self._string_arg('carbonationMethod')
        args = self.parser.parse_args()
        brew = BrewModel()
        Base.set_model_attributes(brew, args, dates=['brewDate'])
        BrewModel.save(brew)

        return brew, 201


api.add_resource(BrewList, '/brews', endpoint='brew_list')
api.add_resource(Brew, '/brews/<int:brew_id>')
