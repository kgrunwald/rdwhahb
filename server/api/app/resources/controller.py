from flask_restful import marshal_with
from flask import request
from app import api, db
from app.models import Controller as ControllerModel, Brew as BrewModel
from base import Base
from werkzeug.exceptions import NotFound, Conflict
from datetime import datetime
import re
from authorization import jwt_required


def convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


class Controller(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ControllerModel.fields)
    @jwt_required()
    def put(self, brew_id, controller_id):
        self._bool_arg('active', required=False)
        self._bool_arg('forceRestart', required=False)
        self._float_arg('actuatorId', required=False)
        self._float_arg('sensorId', required=False)
        self._float_arg('scheduleId', required=False)
        self._string_arg('type', required=False)

        args = self.parser.parse_args()
        controller = ControllerModel.get(controller_id)
        if controller.brew_id != brew_id:
            raise NotFound('Controller: ' + str(controller_id) + ' is not in Brew: ' + str(brew_id))

        Base.set_model_attributes(controller, args, skip=['active', 'forceRestart'])

        if 'active' in args.keys() and args['active']:
            force = False if 'forceRestart' not in args.keys() else args['forceRestart']
            controller.start_controller(force_restart=force)
        elif 'active' in args.keys() and args['active'] == False:
            controller.actual_end_time = datetime.now()

        ControllerModel.save(controller)
        return controller

    @marshal_with(ControllerModel.fields)
    def get(self, brew_id, controller_id):
        controller = ControllerModel.get(controller_id)
        if controller.brew_id != brew_id:
            raise NotFound('Controller: ' + controller_id + ' is not in Brew: ' + brew_id)
        return controller

    @jwt_required()
    def delete(self, brew_id, controller_id):
        controller = ControllerModel.get(controller_id)
        if controller.brew_id != brew_id:
            raise NotFound('Controller: ' + controller_id + ' is not in Brew: ' + brew_id)
        db.session.delete(controller)
        db.session.commit()
        return {'message': 'ok'}


class ControllerList(Base):
    def __init__(self):
        Base.__init__(self)

    @marshal_with(ControllerModel.fields)
    def get(self, brew_id):
        brew = BrewModel.get(brew_id)
        controllers = brew.controllers
        print brew.controllers
        if request.args.get('active') == 'true':
            for controller in controllers:
                if controller.active:
                    return controller
            return []
        return controllers

    @marshal_with(ControllerModel.fields)
    @jwt_required()
    def post(self, brew_id):
        self._float_arg('actuatorId')
        self._float_arg('sensorId')
        self._float_arg('scheduleId')
        self._string_arg('type')
        args = self.parser.parse_args()
        brew = BrewModel.get(brew_id)
        for controller in brew.controllers:
            if controller.type == args['type']:
                raise Conflict('This brew already has a ' + args['type'] + ' controller')

        controller = ControllerModel()
        controller.actuator_id = args['actuatorId']
        controller.sensor_id = args['sensorId']
        controller.schedule_id = args['scheduleId']
        controller.type = args['type']
        controller.brew_id = brew_id
        ControllerModel.save(controller)

        return controller, 201

api.add_resource(ControllerList, '/brews/<int:brew_id>/controllers', endpoint='controller_list')
api.add_resource(Controller, '/brews/<int:brew_id>/controllers/<int:controller_id>', endpoint="controller")
