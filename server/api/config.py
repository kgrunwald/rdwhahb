# Define the application directory
import platform
import os
from datetime import timedelta

# Statement for enabling the development environment
DEBUG = True

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define the database - we are working with
# SQLite for this example
mysql_user = 'rdwhahb'
mysql_pass = 'v1ct0rysux'
mysql_port = 3306
mysql_host = 'knewrealitys.com'
SQLALCHEMY_DATABASE_URI = 'mysql://{}:{}@{}:{}/rdwhahb'.format(mysql_user, mysql_pass, mysql_host, mysql_port)

SQLALCHEMY_MIGRATE_REPO = os.path.join(BASE_DIR, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False
DATABASE_CONNECT_OPTIONS = {}

M_POWER_HOST = 'mpower'

STATUS_CAKE_USERNAME = 'kgrunwald1989'
STATUS_CAKE_API_KEY = 'AJuN9uoqF2H1MnYgRZif'

EXOTHERMIC_OFFSET = 0.25
TEMP_ERROR_FACTOR = 1.5
SENSOR_ERROR_TIME_RANGE = timedelta(minutes=5)
SENSOR_ERROR_EXPECTED_READINGS = 4

DATE_FORMAT = '%Y-%m-%dT%H:%M:%S'

API_KEY = 'b00cdc83-6bd0-45bb-a6ac-2f8ef357ecac'
SECRET_KEY = 'a8e77ee4-7260-4dee-a261-2ac4e5d1792d'

JWT_EXPIRATION_DELTA = timedelta(hours=2)
JWT_AUTH_URL_RULE = '/auth'
JWT_AUTH_USERNAME_KEY = 'email'
JWT_NOT_BEFORE_DELTA = timedelta(seconds=0)

if platform.system() == 'Darwin':
    URL_BASE = 'http://127.0.0.1:5000'
    RPI_URL = 'http://127.0.0.1:6000'
else:
    URL_BASE = 'https://ferment.knewrealitys.com'
    RPI_URL = 'https://pi1.knewrealitys.com'
