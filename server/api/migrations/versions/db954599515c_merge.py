"""merge

Revision ID: db954599515c
Revises: ('9da824ba0798', 'e13f133676f1')
Create Date: 2016-06-15 17:47:07.325459

"""

# revision identifiers, used by Alembic.
revision = 'db954599515c'
down_revision = ('9da824ba0798', 'e13f133676f1')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
