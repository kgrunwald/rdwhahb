"""empty message

Revision ID: 0a97bd9db8bd
Revises: fe97b0873ec5
Create Date: 2016-06-10 16:17:58.402504

"""

# revision identifiers, used by Alembic.
revision = '0a97bd9db8bd'
down_revision = 'fe97b0873ec5'

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.add_column('actuator', sa.Column('cycle_delay', sa.Integer(), nullable=True))
    op.add_column('actuator', sa.Column('enabled', sa.Boolean(), nullable=True))
    op.add_column('actuator', sa.Column('last_activate_time', sa.DateTime(), nullable=True))
    op.add_column('actuator', sa.Column('last_deactivate_time', sa.DateTime(), nullable=True))
    op.add_column('actuator', sa.Column('min_active_time', sa.Integer(), nullable=True))
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('actuator', 'min_active_time')
    op.drop_column('actuator', 'last_deactivate_time')
    op.drop_column('actuator', 'last_activate_time')
    op.drop_column('actuator', 'enabled')
    op.drop_column('actuator', 'cycle_delay')
    ### end Alembic commands ###
