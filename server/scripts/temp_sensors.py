from w1thermsensor import W1ThermSensor
import requests
import ConfigParser
from datetime import datetime

print '\n\n'
print datetime.now()

configs = ConfigParser.ConfigParser()
configs.read('config.ini')

for sensor_id in configs.sections():
    sensor = W1ThermSensor(W1ThermSensor.THERM_SENSOR_DS18B20, sensor_id)
    temperature_in_fahrenheit = sensor.get_temperature(W1ThermSensor.DEGREES_F)

    response = requests.post(configs.get(sensor_id, 'url'), data={'value': temperature_in_fahrenheit},
                             headers={'X-API-Key': 'b00cdc83-6bd0-45bb-a6ac-2f8ef357ecac'})
    print 'Sensor: %s  Value: %r  Response: %r' % (sensor_id, temperature_in_fahrenheit, response)
