from flask_restful import Resource, reqparse


class Base(Resource):
    def __init__(self):
        self.parser = reqparse.RequestParser()

    def __add_arg(self, name, arg_type, help_str, required, location=None):
        if location is None:
            location = ['json', 'form', 'args']
        help_val = name + " was not provided" if help_str is None else help_str
        self.parser.add_argument(name, type=arg_type, help=help_val, required=required, location=location)

    def _string_arg(self, name, help_str=None, required=True):
        self.__add_arg(name, str, help_str, required)

    def _bool_arg(self, name, help_str=None, required=True):
        self.__add_arg(name, bool, help_str, required)

    def _float_arg(self, name, help_str=None, required=True):
        self.__add_arg(name, float, help_str, required)

    def _custom_arg(self, name, arg_type, help_str=None, required=True):
        self.__add_arg(name, arg_type, help_str, required)
