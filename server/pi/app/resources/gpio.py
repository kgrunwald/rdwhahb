from app import api, app
from base import Base
from flask import request
from werkzeug.exceptions import Unauthorized
from werkzeug.security import safe_str_cmp
from functools import wraps
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)


def api_key_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if request.headers.get('X-API-Key') is None:
            raise Unauthorized('API Key not provided.')
        if not safe_str_cmp(request.headers.get('X-API-Key'), app.config['API_KEY']):
            raise Unauthorized('API Key not provided.')
        return f(*args, **kwargs)

    return decorated_function


class Gpio(Base):
    def __init__(self):
        Base.__init__(self)

    @api_key_required
    def put(self, gpio_pin):
        self._bool_arg('state')
        args = self.parser.parse_args()
        GPIO.setup(gpio_pin, GPIO.OUT)
        GPIO.output(gpio_pin, args['state'])
        return {'message': 'ok'}, 200


api.add_resource(Gpio, '/gpio/<int:gpio_pin>')
