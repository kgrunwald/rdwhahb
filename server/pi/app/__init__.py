from flask import Flask
from flask_cors import CORS
from flask_restful import Api

app = Flask(__name__)
CORS(app)
app.config.from_object('config')
api = Api(app)

from app import resources
