#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

export WORKON_HOME=~/.virtualenvs
grep -q -F 'export WORKON_HOME=~/.virtualenvs' ~/.bashrc || echo 'export WORKON_HOME=~/.virtualenvs' >> ~/.bashrc

. /usr/local/bin/virtualenvwrapper.sh
grep -q -F '. /usr/local/bin/virtualenvwrapper.sh' ~/.bashrc || echo '. /usr/local/bin/virtualenvwrapper.sh' >> ~/.bashrc

mkdir -p /root/.virtualenvs

ln -s $DIR/controller.conf /etc/init/controller.conf || :

apt-get -y install python-pip python-dev upstart
pip install virtualenv virtualenvwrapper

if [ ! -d "/root/.virtualenvs/rdwhahb" ]; then
    mkvirtualenv rdwhahb
fi

set +e
workon rdwhahb
set -e

pip install -r $DIR/server/api/requirements.txt